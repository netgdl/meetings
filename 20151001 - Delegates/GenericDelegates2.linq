<Query Kind="Program" />

void Main()
{
	Console.WriteLine("Resultado: {0}", Transformar.Sqr(1));
	Console.WriteLine("Resultado: {0}", Transformar.Upp("hola"));
	Console.WriteLine("Resultado: {0}", Transformar.Positive(1));
	Console.WriteLine("Resultado: {0}", Transformar.Positive(-1));
}
public static class Transformar{

	public static int Sqr(int entrada){
		return entrada * entrada;
	}
	
	public static string Upp(string entrada){
		return entrada.ToUpper();
	}
	
	public static bool Positive(int entrada){
		return entrada >= 0;
	}

}
// Define other methods and classes here
