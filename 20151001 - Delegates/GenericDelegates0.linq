<Query Kind="Program" />

void Main()
{
	Console.WriteLine("Transformed value: {0}", Transformer.Transform(15, 
	t => t * t
	));
	Console.WriteLine("Transformed value: {0}", Transformer.Transform("a", 
	t => t.ToUpper()
	));
	Console.WriteLine("Transformed value: {0}", Transformer.Transform(1, 
	t => t >= 0
	));
}

// Define other methods and classes here
public static class Transformer
{
 
public static TOut Transform<TIn, TOut>(TIn value, Func<TIn,TOut> Transform){
	Console.WriteLine("Receiving: {0}", value) ;
	TOut result = Transform(value);
	return result;
}
 
}