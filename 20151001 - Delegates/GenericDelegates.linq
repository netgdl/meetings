<Query Kind="Program" />

void Main()
{
	Console.WriteLine("Transformed value: {0}", Transformer.Transform(1, t => t.ToString()));
	Console.WriteLine("Transformed value: {0}", Transformer.Transform("a", t => t.ToUpper()));
	
	
	Func<int, string> ConvertDelegate = a => a.ToString() + Math.Exp(a).ToString();
	//multicast
	ConvertDelegate += b => b.ToString();
	
	Console.WriteLine("Transformed value: {0}", Transformer.Transform<int,string >(1, ConvertDelegate));
	
}



public static class Transformer
{
 
public static TOut Transform<TIn, TOut>(TIn value, Func<TIn,TOut> Transform){
	Console.WriteLine("Receiving: {0}", value) ;
	TOut result = Transform(value);
	return result;
}
 
}