<Query Kind="Program" />

void Main()
{

	Action logger = () => Console.WriteLine("Log in file");
	
	//multicast
	logger += () => Console.WriteLine("Log in memory");
	logger += () => Console.WriteLine("Log in database");
	
	
	
	ErrorHandler.Handle<bool>(new Exception(), logger);
}
public static class ErrorHandler{

	public static void Handle<T>(Exception e, Action logger){
	//hacer algo con e
		logger();
	}

}
// Define other methods and classes here
