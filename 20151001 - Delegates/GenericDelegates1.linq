<Query Kind="Program" />

void Main()
{
	Console.WriteLine("Resultado: {0}", Transformar.Transform(1, 
			a => ((int)a*(int)a).ToString()
		)
	);
	Console.WriteLine("Resultado: {0}", Transformar.Transform("hola", 
			a => a.ToString().ToUpper()
		)
	);
	Console.WriteLine("Resultado: {0}", Transformar.Transform(1, 
			a => ((int)a >= 0).ToString()
		)
	);
	Console.WriteLine("Resultado: {0}", Transformar.Transform(-1, 
			a => ((int)a >= 0).ToString()
		)
	);
}
public static class Transformar{
	
	public delegate string TransformDel(object entrada);//Func<Tin, Tout>
	public delegate void TransformVoid(object entrada);//Action<T>
	public static string Transform(object entrada, TransformDel t){
		return t(entrada);
	}

}
// Define other methods and classes here
